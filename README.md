[![forthebadge made-with-python](https://forthebadge.com/images/badges/made-with-python.svg)](https://www.python.org/)  [![Generic badge](https://img.shields.io/badge/Python-3.8-<red>.svg)](https://shields.io/)
## Insertion de valeurs dans des listes

Algorithme python pour insérer des valeurs (int) dans des listes simples et chaînés

## Ce fichier contient:
- Le code pour créer une liste chainé en orienté objet
- Les fonctions:
    - Deux fonctions natives pour aller au suivant et fixer une valeur
    - Pour créer une liste
    - Afficher la liste
    - Pour ajouter une valeur à la liste
    - Effacer un élément de la liste (par son rang et non sa valeur)
    - Supprimer une valeur (par son rang et non sa valeur)
    - Récupérer une valeur par son rang
- Le code pour insérer une valeur dans une liste normale de type:

> liste=[2,3,4,5,7,8]